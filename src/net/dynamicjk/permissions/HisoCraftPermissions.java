/*

		HisoPermissions Main Class Instance


 */

package net.dynamicjk.permissions;

import net.dynamicjk.permissions.api.HisoCraftPermissionsAPI;
import net.dynamicjk.permissions.commands.PermissionCommand;
import net.dynamicjk.permissions.listeners.PlayerJoinListener;
import net.dynamicjk.permissions.listeners.PlayerLeaveListener;
import net.dynamicjk.permissions.permissions.Attachments;
import net.dynamicjk.permissions.permissions.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class HisoCraftPermissions extends JavaPlugin {

	private HisoCraftPermissionsAPI permissionsAPI;
	private Permissions perm;
	private Attachments attach;


	/**
	 * The Enable method for the entire HisoCraft Permissions plugin.
	 * The Enable messages are used to determine where a failure has occured in the
	 * start up process.
	 *
	 * First setup API, and attachments from there load the config file
	 * then setup the commands and their executor class.
	 * Register the Listeners.
	 *
	 * if all is successful the plugin will enable.
	 *
	 *
	 *
	 */
	public void onEnable() {
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &eEnabling..."));
		setPermissionsAPI(new HisoCraftPermissionsAPI(this));
		perm = new Permissions(this);
		attach = new Attachments(this);
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &eLoading config..."));
		saveDefaultConfig();
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &aComplete!"));
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &eLoading commands..."));
		getCommand("perm").setExecutor(new PermissionCommand(this));
		getCommand("perms").setExecutor(new PermissionCommand(this));
		getCommand("setrank").setExecutor(new PermissionCommand(this));
		getCommand("ranks").setExecutor(new PermissionCommand(this));
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &aComplete!"));
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &eRegistering Listeners..."));
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerLeaveListener(this), this);
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &aComplete!"));
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &ahas enabled and is running."));
	}

	/**
	 * This method runs the code that will execute when the plugin has been disabled
	 * we do not use this method because java's garbage collector automatically
	 * releasede the memory being utilized by the plugin.
	 */
	public void onDisable() {
		getServer().getConsoleSender().sendMessage(colo("&eHiso&fCraft&bPermissions &cDisabled."));
	}

	/**
	 * Return the Permissions class that handles all permission
	 * transactions.
	 * @return Permissions.
	 */
	public Permissions getPermissionsClass() {
		return perm;
	}

	/**
	 * Return the Attachments class with stores the hashmap cache, which is
	 * the Permissions attached to players.
	 * @return
	 */
	public Attachments getAttachments() {
		return attach;
	}

	/**
	 * Return the HisoCraft API in case it needs to be accessed internally by the plugin.
	 * @return HisoAPI.
	 */
	public HisoCraftPermissionsAPI getHisoAPI() {
		return permissionsAPI;
	}

	/**
	 * Set the permissions API variable to a new instance.
	 * @param perms Permissions API class
	 */
	public void setPermissionsAPI(HisoCraftPermissionsAPI perms) {
		permissionsAPI = perms;
	}

	/**
	 * String colorizer
	 * @param s String to be colorized
	 * @return colorized String.
	 */
	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
}
