/*

		HisoPermissions API Class Instance


 */

package net.dynamicjk.permissions.api;

import net.dynamicjk.permissions.HisoCraftPermissions;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class HisoCraftPermissionsAPI {

	private static HisoCraftPermissions permissions;

	/**
	 * HisoCraft Permission API constructor.
	 * @param permissions main class instance.
	 */
	public HisoCraftPermissionsAPI(HisoCraftPermissions permissions) {
		HisoCraftPermissionsAPI.permissions = permissions;
	}

	/**
	 * Method that returns instance of main class.
	 * @return main class instance.
	 */
	public static HisoCraftPermissions getHisoCraftPermissions() {
		return permissions;
	}
}
