/*

		HisoPermissions Command Class Instance.


 */

package net.dynamicjk.permissions.commands;

import net.dynamicjk.permissions.HisoCraftPermissions;
import net.dynamicjk.permissions.rankget.Ranks;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class PermissionCommand implements CommandExecutor {

	private Ranks rank;

	private HisoCraftPermissions main;

	/**
	 * Permissions command class constructor.
	 * @param perms main class instance.
	 */
	public PermissionCommand(HisoCraftPermissions perms) {
		this.main = perms;
		this.rank = new Ranks();
	}

	/**
	 *
	 * The command execetor that operates all of the commands within the hisocraft permissions.
	 * plugin.
	 *
	 * @param sender The sender of the command such as console or player.
	 * @param cmd The command structure.
	 * @param commandLabel Command Label -> never used.
	 * @param args Command arguments such as player and rank after /perm player rank.
	 * @return wether the command was executed correctly.
	 */
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ranks")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(colo("&cYou must be a player to run this command!"));
				return true;
			}
			Player p = (Player)sender;
			if (CoreAPI.getRanks().hasSuperiorSystemPermissions(p)) {
				p.sendMessage(colo("      &e&lHiso&f&lPermissions      "));
				p.sendMessage(" ");
				p.sendMessage(colo("&cOWNER"));
				p.sendMessage(colo("&cADMIN"));
				p.sendMessage(colo("&3DEV"));
				p.sendMessage(colo("&2MOD"));
				p.sendMessage(colo("&bHELPER"));
				p.sendMessage(colo("&6BUILDER"));
				p.sendMessage(colo("&6YOUTUBER"));
				p.sendMessage(colo("&9STREAMER"));
				p.sendMessage(colo("&dULTRA"));
				p.sendMessage(colo("&6ELITE"));
				p.sendMessage(colo("&bEPIC"));
				p.sendMessage(colo("&aVIP"));
				p.sendMessage(" ");
				p.sendMessage("&eOrdered based on network authority.");
				p.sendMessage(" ");
			}
		}
		if (cmd.getName().equalsIgnoreCase("perm") || cmd.getName().equalsIgnoreCase("perms")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(colo("&cYou must be a player to run this command!"));
				return true;
			}
			Player p = (Player)sender;
			if (CoreAPI.getRanks().hasSuperiorSystemPermissions(p)) {
				if (args.length == 0) {
					p.sendMessage(colo("      &e&lHiso&f&lCraft&b&lPermissions      "));
					p.sendMessage(colo(" "));
					p.sendMessage(colo("&a/perm add <permission> <rank>"));
					p.sendMessage(colo("&7Add a permission to an existing rank"));
					p.sendMessage(colo(" "));
					p.sendMessage(colo("&a/ranks"));
					p.sendMessage(colo("&7View all usable rank names"));
					p.sendMessage(colo(" "));
					p.sendMessage(colo("&a/perm adddefault <permission> <rank>"));
					p.sendMessage(colo("&7Add a local permission to all existing ranks"));
					p.sendMessage(colo(" "));
					p.sendMessage(colo("&a/perm create <rank>"));
					p.sendMessage(colo("&7Add a rank to the config"));
					p.sendMessage(colo("&7You cannot create new ranks, this command only adds"));
					p.sendMessage(colo("&7the rank to the configuration to add local permissions"));
					p.sendMessage(colo(" "));
					p.sendMessage(colo("&a/perm remove <permission> <rank>"));
					p.sendMessage(colo("&7Remove a permission from a rank."));
					p.sendMessage(" ");
					p.sendMessage(colo("&a/perm createglobalrank <rank>>"));
					p.sendMessage(colo("&7Adds a rank to the core database (You cannot create custom ranks)"));
					p.sendMessage(" ");
					p.sendMessage(colo("&a/perm addglobalperm <permission> <rank>"));
					p.sendMessage(colo("&7Add a permission from a rank in entire network"));
					p.sendMessage(" ");
					p.sendMessage(colo("&a/perm removeglobalperm <permission> <rank>"));
					p.sendMessage(colo("&7Remove a permission from a rank in entire network"));
					p.sendMessage(colo(" "));
					p.sendMessage(colo("&a/perm reload"));
					p.sendMessage(colo("&7Reload permissions config file."));
					p.sendMessage(colo(" "));
					return true;
				}
				if (args[0].equalsIgnoreCase("add")) {
					if (args.length <= 2) {
						p.sendMessage(colo("&a/perm add <permission> <rank>"));
						p.sendMessage(colo("&7Add a permission to an existing rank"));
						return true;
					}
					String perm = args[1];
					String rank = args[2].toUpperCase();
					this.main.getPermissionsClass().addPermission(rank, perm);
					p.sendMessage(colo("&6&l>> &7Added Permission: &8" + perm + " &7to rank: &8" + rank));
					return true;
				}
			} else {
				p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &aversion 2.0 by Zumorik."));
				return true;
			}
			if (args[0].equalsIgnoreCase("adddefault")) {
				if (args.length < 2) {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &a/perm adddefault <permission>"));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Add a local global permission to all ranks"));
					return true;
				}
				String perm = args[1];
				this.main.getPermissionsClass().addDefaultPermission(perm);
				p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Added Default Permission: &8" + perm));
				return true;
			}
			if (args[0].equalsIgnoreCase("reload") &&
					CoreAPI.getRanks().hasSuperiorSystemPermissions(p)) {
				p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &ePermissions reloaded"));
				this.main.reloadConfig();
			}
			if (args[0].equalsIgnoreCase("remove")) {
				if (args.length <= 2) {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &a/perm remove <perm> <rank>"));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Remove a ranks permission."));
					return true;
				}
				String perm = args[1];
				String rank = args[2].toUpperCase();
				this.main.getPermissionsClass().addPermission(rank, perm);
				p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Removed Permission: &8" + perm + " &7to rank: &8" + rank));
				return true;
			}
			if (args[0].equalsIgnoreCase("hasperm")) {
				Player t = Bukkit.getPlayer(args[1]);
				if (t.hasPermission(args[2])) {
					p.sendMessage("&e&lHiso&f&lCraft&b&lPermissions &aYes permission is enabled.");
				} else {
					p.sendMessage("&e&lHiso&f&lCraft&b&lPermissions &cNo permission is not enabled.");
				}
			}
			if (args[0].equalsIgnoreCase("create")) {
				if (args.length <= 1) {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &e- /perm create <rank_id>"));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Create a rank"));
					return true;
				}
				String rank = args[1].toUpperCase();
				if (this.main.getConfig().getConfigurationSection(rank + ".Permissions") == null) {
					this.main.getConfig().set(rank + ".Permissions", "");
					if (this.main.getConfig().getBoolean("useGlobalPerms"))
						Bukkit.getScheduler().runTaskAsynchronously((Plugin)this.main, () -> CoreAPI.getCore().getRankDatabase().createRank(rank));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &eCreated rank &8" + rank));
				} else {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &c" + rank + " already exists."));
				}
				return true;
			}
			if (args[0].equalsIgnoreCase("createglobalrank")) {
				if (args.length <= 1) {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &e- /perm createglobalrank <rank_id>"));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Create a global rank"));
					return true;
				}
				String rank = args[1].toUpperCase();
				Bukkit.getScheduler().runTaskAsynchronously((Plugin)this.main, () -> CoreAPI.getCore().getRankDatabase().createRank(rank));
				p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &eCreated global rank &8" + rank));
				return true;
			}
			if (args[0].equalsIgnoreCase("addglobalperm")) {
				if (args.length <= 1) {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &e- /perm addglobalperm <rank> <perm>"));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Add a global rank permission"));
					return true;
				}
				String rank = args[1].toUpperCase();
				String perm = args[2].toLowerCase();
				Bukkit.getScheduler().runTaskAsynchronously((Plugin)this.main, () -> {
					if (!CoreAPI.getCore().getRankDatabase().getGlobalPerms(Rank.valueOf(rank)).contains(perm)) {
						CoreAPI.getCore().getRankDatabase().addGlobalPerm(rank, perm);
						p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Added Permission: &8" + perm + " &7to rank: &8" + rank));
					} else {
						p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &8" + rank + " &7already has the permission: &8" + perm));
					}
				});
				return true;
			}
			if (args[0].equalsIgnoreCase("removeglobalperm")) {
				if (args.length <= 1) {
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &e- /perm removeglobalperm <rank> <perm>"));
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Remove a global rank permission"));
					return true;
				}
				String rank = args[1].toUpperCase();
				String perm = args[2].toLowerCase();
				Bukkit.getScheduler().runTaskAsynchronously((Plugin)this.main, () -> {
					CoreAPI.getCore().getRankDatabase().removeGlobalPerm(rank, perm);
					p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Removed Permission: &8" + perm + " &7to rank: &8" + rank));
				});
				return true;
			}
		}
		if (cmd.getName().equalsIgnoreCase("setrank"))
			if (sender instanceof Player) {
				Player p = (Player)sender;
				if (CoreAPI.getRanks().hasSuperiorSystemPermissions(p)) {
					if (args.length <= 1) {
						p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &e- /setrank <Player> <rank>"));
						p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Set a players rank, use /ranks to see ranks"));
						return true;
					}
					String target = args[0];
					if (Bukkit.getPlayer(target) != null) {
						Player t = Bukkit.getPlayer(target);
						try {
							CoreAPI.getRanks().setRank(t, Rank.valueOf(args[1].toUpperCase()));
							this.main.getPermissionsClass().removeInGamePerms(t,
									Rank.valueOf(args[1].toUpperCase()).toString().toUpperCase());
							p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &ePlayers rank has been set to: &b" + args[1]));
							t.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &eYour rank has been set to: &b" + args[1]));
						} catch (IllegalArgumentException e) {
							p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &cThe rank &b" + args[1] + " &cdoes not exist!"));
							e.printStackTrace();
						}
					} else {
						Bukkit.getScheduler().runTaskAsynchronously((Plugin)this.main, () -> {
							CoreAPI.getCore().getRankDatabase().setRank(Bukkit.getOfflinePlayer(args[0]), Rank.valueOf(args[1].toUpperCase()).toString());
							p.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &ePlayers rank has been set to: &b" + args[1]));
						});
						return true;
					}
				}
			} else {
				if (args.length <= 1) {
					sender.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &e- /setrank <Player> <rank>"));
					sender.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &7Set a players rank"));
					return true;
				}
				String target = args[0];
				if (Bukkit.getPlayer(target) != null) {
					try {
						Player t = Bukkit.getPlayer(target);
						CoreAPI.getRanks().setRank(t, Rank.valueOf(args[1].toUpperCase()));
						this.main.getPermissionsClass().removeInGamePerms(t,
								Rank.valueOf(args[1].toUpperCase()).toString().toUpperCase());
						sender.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &ePlayers rank has been set to: &b" + args[1]));
						t.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &eYour rank has been set to: &b" + args[1]));
					} catch (IllegalArgumentException e) {
						sender.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &cThe rank &b" + args[1] + " &cdoes not exist!"));
						e.printStackTrace();
					}
				} else {
					sender.sendMessage(colo("&e&lHiso&f&lCraft&b&lPermissions &cUnknown player!"));
					return true;
				}
			}
		return false;
	}

	/**
	 * String colorizer
	 * @param s String to be colorized
	 * @return colorized String.
	 */
	public String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
}