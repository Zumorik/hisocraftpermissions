/*

		HisoPermissions PlayerJoinListener Class Instance.


 */

package net.dynamicjk.permissions.listeners;

import com.nametagedit.plugin.NametagEdit;
import java.sql.SQLException;
import java.util.ConcurrentModificationException;
import net.dynamicjk.permissions.HisoCraftPermissions;
import net.dynamicjk.permissions.tags.PlayerTags;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class PlayerJoinListener implements Listener {

	private PlayerTags tags;
	private HisoCraftPermissions main;

	/**
	 * Player Join Listener class Constructor.
	 * @param perms main class instance.
	 */
	public PlayerJoinListener(HisoCraftPermissions perms) {
		this.main = perms;
		this.tags = new PlayerTags(main);
	}

	/**
	 *
	 * The Event that setups the players permissions when they join.
	 * First creates the default tab color in case there is a failure
	 * Use the nametag api to set the players tab color to default.
	 * create a async threat that will execute the database call and
	 * set up the players permissions and then actual rank color in tab and prefix
	 * send join message that is set up within the config.
	 *
	 * @param e event.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	@EventHandler
	public void onJoin(PlayerJoinEvent e) throws ClassNotFoundException, SQLException {
		Player p = e.getPlayer();
		e.setJoinMessage(null);
		String tab = "&e";
		NametagEdit.getApi().setPrefix(p.getName(), colo(tab));
		Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
			try {
				if (CoreAPI.getCore().getRankDatabase().getRank(p) != null) {
					String rank = CoreAPI.getCore().getRankDatabase().getRank(p).toUpperCase();
					main.getPermissionsClass().setPermissions(p, rank);
					playJoinMessage(p, rank);
					tags.setupTagsJoin(p, rank);
				}
			} catch (SQLException | ConcurrentModificationException | ClassNotFoundException var3) {
			}

		});
	}

	/**
	 * Returns a String to be colorized.
	 * @param s String to be colorized.
	 * @return Colorized String.
	 */
	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	private void playJoinMessage(Player p, String rank) {
		String message = main.getConfig().getString("JoinMessages." + rank);
		if ((message != null) && !message.equalsIgnoreCase("none")) {
			Bukkit.broadcastMessage(colo(message.replaceAll("%player%", p.getName())));
		}

	}
}
