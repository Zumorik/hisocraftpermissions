/*

		HisoPermissions PlayerLeaveListener Class Instance.


 */

package net.dynamicjk.permissions.listeners;

import net.dynamicjk.permissions.HisoCraftPermissions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class PlayerLeaveListener implements Listener {

	private HisoCraftPermissions main;

	/**
	 *
	 * @param main
	 */
	public PlayerLeaveListener(HisoCraftPermissions main) {
		this.main = main;
	}

	/**
	 *
	 * @param e
	 */
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		e.setQuitMessage(null);
		main.getPermissionsClass().removePermissions(p);
	}
}
