/*

		HisoPermissions Permissions Attachment Handler Class Instance.


 */

package net.dynamicjk.permissions.permissions;

import java.util.HashMap;
import java.util.UUID;
import net.dynamicjk.permissions.HisoCraftPermissions;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class Attachments {

	private HisoCraftPermissions main;
	private HashMap<UUID, PermissionAttachment> attach;

	/**
	 * Attachment class constructor.
	 * @param main main class instance.
	 */
	public Attachments(HisoCraftPermissions main) {
		this.main = main;
		attach = new HashMap<>();
	}

	/**
	 * Return the Hashmap that stores the players uuids along with their attached permissions.
	 * @return HashMap with Permission Attachments.
	 */
	public HashMap<UUID, PermissionAttachment> getAttachments() {
		return attach;
	}

	/**
	 * Get the specific player permission attachment
	 * Check if it exists within the stored hashmap if not create a permission attachment for the
	 * player and store it within the hashmap
	 * if it does exist then just return that players permission attachment.
	 * @param p Player.
	 * @return Permission attachment.
	 */
	public PermissionAttachment getPlayerAttachments(Player p) {
		if (!main.getAttachments().getAttachments().containsKey(p.getUniqueId())) {
			PermissionAttachment attach = p.addAttachment(main);
			main.getAttachments().getAttachments().put(p.getUniqueId(), attach);
		}
		return main.getAttachments().getAttachments().get(p.getUniqueId());
	}
}
