/*

		HisoPermissions Permission Handler Class Instance.


 */

package net.dynamicjk.permissions.permissions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.dynamicjk.permissions.HisoCraftPermissions;
import net.dynamicjk.permissions.tags.PlayerTags;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class Permissions {

	private PlayerTags tags;
	private HisoCraftPermissions main;

	/**
	 * Permissions class constructor.
	 * @param main main class instance.
	 */
	public Permissions(HisoCraftPermissions main) {
		this.main = main;
		this.tags = new PlayerTags(main);
	}

	/**
	 *
	 * This method returns the Permissions of a rank stored within the local config file of the plugin.
	 * First create an empty list and then store the config data.
	 *
	 * @param p Player
	 * @param rank Players rank as a String.
	 * @return return a list of all of the ranks permissions.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<String> getConfigPermissions(Player p, String rank) throws ClassNotFoundException, SQLException {
		List<String> perms = new ArrayList<String>();
		try {
			if (main.getConfig().getStringList(rank + ".Permissions") != null)
				perms = main.getConfig().getStringList(rank + ".Permissions");
		} catch (NullPointerException e) {
			perms = new ArrayList<String>();
			e.printStackTrace();
		}
		return perms;
	}

	/**
	 * Get a List of a specific ranks permissions from the local config file of the plugin.
	 * @param rank String of rank.
	 * @return list of all permissions.
	 */
	public List<String> getRankPermissions(String rank) {
		return main.getConfig().getStringList(rank + ".Permissions");
	}

	/**
	 * Get the default permissions as a list from the local config file of the plugin.
	 * These are the permissions that all ranks will share.
	 * @return list of all default permissions.
	 */
	public List<String> getDefaultPermissions() {
		return main.getConfig().getStringList("Default.Permissions");
	}

	/**
	 * Add a default permission that will be shared with all ranks
	 * List create a list with the local default permissions
	 * add the permission to the list
	 * save the list to the local config file of the plugin.
	 * save config
	 * @param perm permission to be added to the default permissions section.
	 */
	public void addDefaultPermission(String perm) {
		List<String> permList = getDefaultPermissions();
		permList.add(perm);
		main.getConfig().set("Default.Permissions", permList);
		main.saveConfig();
	}

	/**
	 * This loads the players permission based on what rank they are.
	 * First load all of the local ranks permissions onto the player.
	 * Then load all of the local default permissions onto the plauer.
	 * And if enabled load all of the database permissions onto the player by
	 * creating an array (a list) of permissions based on the string you get from the
	 * database. For example database may return "gamemode.creative,gamemode.survival" the
	 * code will split it into
	 *
	 * gamemode.creative
	 * gamemode.survival
	 *
	 * it splits it like this at every comma within the returned data from database.
	 * All of this data is then loaded into the players permission attachment.
	 *
	 * and finally load whether this player is an op.
	 *
	 * @param p Player.
	 * @param rank Players rank as a string.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void setPermissions(Player p, String rank) throws ClassNotFoundException, SQLException {
		PermissionAttachment attachment = p.addAttachment(main);
		for (String s : getConfigPermissions(p, rank))
			attachment.setPermission(s, true);
		for (String s : getDefaultPermissions())
			attachment.setPermission(s, true);
		if (main.getConfig().getBoolean("useGlobalPerms")) {
			String perms = CoreAPI.getCore().getRankDatabase().getGlobalPerms(Rank.valueOf(rank));
			if (perms != null) {
				String[] aperms = perms.split(",");
				for (int i = 0; i < aperms.length; i++) {
					if (!aperms[i].equals("") || !aperms[i].equals(" ") || aperms[i] != null)
						attachment.setPermission(aperms[i].trim(), true);
				}
			}
		}
		main.getAttachments().getAttachments().put(p.getUniqueId(), attachment);
		setOP(p, rank);
	}

	/**
	 * Sets the player as an operator based on whether the config file
	 * states that the rank is an op.
	 * Example config
	 *
	 * Owner:
	 *    op: true
	 *
	 * The Owner rank is granted operator.
	 *
	 * @param p Player.
	 * @param rank The players rank as a string.
	 */
	private void setOP(Player p, String rank) {
		if (main.getConfig().getBoolean(rank + ".op")) {
			p.setOp(true);
		} else {
			p.setOp(false);
		}
	}

	/**
	 * Remove all players permissions from their attached permissions
	 * check if the attachment exists and if they exist within the attachments hashmap storage (cache)
	 * remove permissions
	 * @param p
	 */
	public void removePermissions(Player p) {
		try {
			if (main.getAttachments().getPlayerAttachments(p) != null && main.getAttachments().getAttachments().containsKey(p.getUniqueId())) {
				PermissionAttachment attachment = main.getAttachments().getPlayerAttachments(p);
				p.removeAttachment(attachment);
			}
		} catch (IllegalArgumentException ignored) {}
	}

	/**
	 * Remove a players rank permissions based on what rank they are or have been moved to.
	 * Run an async threat to the database to reset ALL players permissions like if they
	 * had just joined the server. And finally reset the players tags and prefix.
	 * @param p Player.
	 * @param rank The players rank.
	 */
	public void removeInGamePerms(Player p, String rank) {
		try {
			for (PermissionAttachmentInfo s : p.getEffectivePermissions())
				removePermission(p, s.getPermission());
			Bukkit.getScheduler().runTaskAsynchronously((Plugin)this.main, () -> {
				try {
					main.getPermissionsClass().setPermissions(p, rank);
					tags.setupTags(p);
				} catch (ClassNotFoundException|SQLException e) {
					e.printStackTrace();
				}
			});
		} catch (IllegalArgumentException ignored) {}
	}

	/**
	 *
	 * Remove a specific Permission from a players attached permissions
	 * get the permissions attachment from the hashmap cache and
	 * remove permission.
	 *
	 * @param player Player.
	 * @param permission The permission that will be removed from the player.
	 */
	public void removePermission(Player player, String permission) {
		PermissionAttachment attachment = main.getAttachments().getPlayerAttachments(player);
		attachment.unsetPermission(permission);
	}

	/**
	 *
	 * Add a permission to the local config file for a rank
	 * Load the current ranks permissions if they exist in the config
	 * and then add the permission to the list and save that list
	 * to the config file.
	 *
	 * @param rank the rank to add permissions to.
	 * @param perm the permission being added.
	 */
	public void addPermission(String rank, String perm) {
		List<String> perms = getRankPermissions(rank);
		if (!perms.contains(perm)) {
			perms.add(perm);
			main.getConfig().set(rank + ".Permissions", perms);
			main.saveConfig();
		}
	}

	/**
	 * Remove a permission from a ranks local file.
	 * Load the permissions as a list for that rank
	 * remove the permission and then save that list
	 * to the config file.
	 * @param rank the rank to remove permissions.
	 * @param perm the permission being removed.
	 */

	public void removePermission(String rank, String perm) {
		List<String> perms = getRankPermissions(rank);
		if (perms.contains(perm)) {
			perms.remove(perm);
			main.getConfig().set(rank + ".Permissions", perms);
			main.saveConfig();
		}
	}

	/**
	 *
	 * IMPORTANT NOTE
	 * ALL DATABASE PERMISSION ADDING AND REMOVING IS DONE WITHIN THE
	 * PERMISSIONS COMMAND CLASS AND IS HANDLED BY THE CORE NOT THE
	 * PERMISSIONS PLUGIN ITSELF.
	 *
	 */
}