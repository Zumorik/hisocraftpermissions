/*

		HisoPermissions Rank Handler Class Instance.


 */

package net.dynamicjk.permissions.rankget;

import java.sql.SQLException;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.rank.Rank;
import org.bukkit.entity.Player;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class Ranks {

    /**
     * Return the players rank from the core API
     * This is just a method used to make the code look easier to read
     * in other classes.
     * @param p Player.
     * @return the players rank.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Rank getRank(Player p) throws ClassNotFoundException, SQLException {
        return CoreAPI.getRanks().getRank(p);
    }
}
