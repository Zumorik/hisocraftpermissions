/*

		HisoPermissions Player Tag Class Instance.


 */

package net.dynamicjk.permissions.tags;

import com.nametagedit.plugin.NametagEdit;
import net.dynamicjk.permissions.HisoCraftPermissions;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 *
 * @author John Gomez
 * Copyright C ZumorikDev 2020
 *
 */

public class PlayerTags {

    private HisoCraftPermissions main;

    /**
     * Player Tags class constructor.
     * @param main main class instance.
     */
    public PlayerTags(HisoCraftPermissions main) {
        this.main = main;
    }

    /**
     * Sets up the players tag and prefix
     * Creates the default rank as default and default
     * prefix and tab color as yellow in case the database
     * is unreachable. and determine wether or not tags are
     * disabled.
     *
     * Works by loading the ranks tab color from the local file
     * and using NameTag API to set the players prefix
     * and using the normal Bukkit API to set the players
     * color within the tab list.
     *
     * Incase database is unreachable or something is configured wrong
     * set the ysers default tab and prefix color to yellow.
     *
     * @param p Player.
     */
    public void setupTags(Player p) {
        String rank = "USER";
        rank = CoreAPI.getRanks().getRank(p).toString().toUpperCase();
        if (!main.getConfig().getBoolean("disableTags")) {
            String tab = "&e";
            try {
                tab = CoreAPI.getCore().getConfig().getString(rank + ".tab");
                NametagEdit.getApi().setPrefix(p.getName(), colo(tab));
                p.setPlayerListName(colo(tab + " " + p.getName()));
            } catch (NullPointerException e) {
                tab = "&e";
                NametagEdit.getApi().setPrefix(p.getName(), colo(tab));
                p.setPlayerListName(colo(tab + " " + p.getName()));
            }
        }
    }

    /**
     * JOIN VERSION OF SETUP TAGS
     * THIS ONE IS USED IN CASE OF A PLAYERS RANK
     * BEING CHANGED IN ORDER TO REJOIN THEM TO THEIR
     * NEW TAB AND PREFIX COLOR.
     *
     * Sets up the players tag and prefix
     * Creates the default rank as default and default
     * prefix and tab color as yellow in case the database
     * is unreachable. and determine wether or not tags are
     * disabled.
     *
     * Works by loading the ranks tab color from the local file
     * and using NameTag API to set the players prefix
     * and using the normal Bukkit API to set the players
     * color within the tab list.
     *
     * Incase database is unreachable or something is configured wrong
     * set the ysers default tab and prefix color to yellow.
     *
     * @param p Player
     * @param rank the players rank.
     */
    public void setupTagsJoin(Player p, String rank) {
        if (!main.getConfig().getBoolean("disableTags")) {
            String tab = "&e";
            try {
                tab = CoreAPI.getCore().getConfig().getString(rank + ".tab");
                NametagEdit.getApi().setPrefix(p.getName(), colo(tab));
                p.setPlayerListName(colo(tab + " " + p.getName()));
            } catch (NullPointerException e) {
                tab = "&e";
                NametagEdit.getApi().setPrefix(p.getName(), colo(tab));
                p.setPlayerListName(colo(tab + " " + p.getName()));
            }
        }
    }

    /**
     * String colorizer
     * @param s String to be colorized
     * @return colorized String.
     */
    private String colo(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}
